# linux-sunxi-rootfs

This project contains configuration files and build instructions for building Debian image for ANTRAX [Olimex A20-SOM](https://www.olimex.com/Products/SOM/A20/A20-SOM/) based hardware.

# Build a Linux distribution

Due to hosting on GitLab there are two ways to build image:

* Automated via GitLab CI
* Manual


## Automated build

Automated build realized by means of GitLab continuous integration. Build is performed in Docker on Ubuntu 14.04 image and require no user intervention.
Build is triggered by push to master and results could be obtained from description of last commit in push.


## Manual build

To avoid installing unnecessary packages in system it is recommended to use Docker with appropriate [image](https://gitlab.com/flamesgroup/build-dockers/tree/master/ubuntu-14.04-rootfs).

## Download prerequisites

To build this image you should compile [u-boot](https://gitlab.com/flamesgroup/u-boot), [linux-sunxi kernel](https://gitlab.com/flamesgroup/linux-sunxi) and [FEX files](https://gitlab.com/flamesgroup/linux-sunxi-fex). Description of build process could be found in appropriate *.gitlab-ci.yml* file in each project. Desired files mentioned in _artifacts_ section.

## Create SD card image

Create an empty file:
```
# dd if=/dev/zero of=debian8_box.img bs=1M count=2000
```

SD card layout described in [sd.layout](sd.layout). For simplifying _build_blank_image.sh_ could be used.

```bash
[root@host ~]# bash -x build_blank_image.sh debian8_box.img
```
It will partition image, map it as a device, create file systems on */boot* and */* partition and write U-Boot (stored in *u-boot-sunxi-with-spl.bin* file). At the end device would be deleted.

## Install base Debian filesystem

Installation of Debian file system divided into two phases: inside host system and inside chrooted environment.

### On host system
```bash
[root@host ~]# mkdir /mnt/{boot,root}
[root@host ~]# bash -x mount_image.sh debian8_box.img
[root@host ~]# debootstrap --arch=armhf --foreign jessie /mnt/root
```

At the end of this steps image will be populated with part of a Debian filesystem. To make it work second phase is needed. 

### In chrooted environment
```bash
[root@host ~]# cp /usr/bin/qemu-arm-static /mnt/root/usr/bin/               # To be able to execute arm binaries on x86 platform
[root@host ~]# chroot /mnt/root /debootstrap/debootstrap --second-stage
[root@host ~]# cp -r ./second_stage.sh /mnt/root/root
[root@host ~]# cp -r fsroot/etc/apt /mnt/root/etc/
[root@host ~]# chroot /mnt/root /bin/bash -x /root/second_stage.sh
[root@host ~]# rm -rf /mnt/root/root/second_stage.sh
```

This will finish first stage of debootstrap, copy correct apt settings and install all required packages.

## Configure packages and cleanup

Last step with Debian packages is configuration. For some packages we ship configuration files. All such files placed under *fsroot* directory.
Target for cleanup:

* `/root/scripts` - configuration scripts that should be executed in chrooted environment
* `/root/packages` - prebuild deb packages that should be installed at build time
* `/usr/bin/qemu-arm-static` - binary that allow execution of arm binaries on x86

```bash
 cp -r ./fsroot/* /mnt/root
 chroot /mnt/root /bin/bash -x /root/scripts/run.sh
 rm -rf /mnt/root/root/scripts
 rm -rf /mnt/root/root/packages
 rm -rf /mnt/root/usr/bin/qemu-arm-static
```

## Install FEX files

FEX files should already built and *antrax_gsmbox.fex.bin* *antrax_simbox.fex.bin* *antrax_commonbox.fex.bin* files should be in place.

```bash
[root@host ~]# ln -s antrax_commonbox.fex.bin antrax_box.fex.bin
[root@host ~]# cp antrax_{box,commonbox,simbox,gsmbox}.fex.bin /mnt/boot
```

## Install kernel
As with FEX files prebuild kernel should be in place before before this stage.

```bash
[root@host ~]#  cp kernel/uImage /mnt/boot
[root@host ~]#  cp -r kernel/lib/* /mnt/root/lib

```

## Unmount image

```bash
[root@host ~]# bash -c "sync; umount -l /mnt/root; umount -l /mnt/boot"
[root@host ~]# sync
[root@host ~]# fuser -v -m /mnt/root
[root@host ~]# kpartx -v -d debian8_box.img
```

## Questions

If you have any questions about **linux-sunxi-rootfs**, feel free to create issue with it.
