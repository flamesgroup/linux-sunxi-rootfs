setenv bootm_boot_mode sec
usb start
ext2load usb 0 0x42000000 uImage && ext2load usb 0 0x43000000 antrax_commonbox.fex.bin && setenv bootargs console=ttyS0,115200 root=/dev/sda2 rootwait panic=10 ${extra} && bootm 0x42000000
ext2load mmc 0 0x42000000 uImage && ext2load mmc0 0 0x43000000 antrax_box.fex.bin && setenv bootargs console=ttyS0,115200 root=/dev/mmcblk0p2 rootwait panic=10 ${extra} && bootm 0x42000000
