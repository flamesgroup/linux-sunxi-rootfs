#!/bin/bash
if [ $(id -u) -ne 0 ]; then
  printf "Script must be run as root. Try 'sudo ./update-java-alternatives.sh'\n"
  exit 1
fi
if [ ! -f "$1/bin/java" ]; then
  echo "It isn't java directory"
  exit 1
else
ln -sf $1 /opt/java/latest
PATH_TO_BIN=/opt/java/latest/bin
for f in `ls $PATH_TO_BIN`; do
  update-alternatives --install "/usr/bin/$f" "$f" "$PATH_TO_BIN/$f" 1
done
fi
EXPORT_STRING="export JAVA_HOME=/opt/java/latest"
if ! grep -Fxq "$EXPORT_STRING" "/etc/bash.bashrc"; then
  echo "export JAVA_HOME=/opt/java/latest" >> /etc/bash.bashrc
fi
