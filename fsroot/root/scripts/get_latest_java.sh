#!/bin/bash
# This script downloads and installs latest java for ARM from Oracle site
# You must accept the Oracle Binary Code License
# http://www.oracle.com/technetwork/java/javase/terms/license/index.html

JDK_VERSION="8"
EXT="tar.gz"
OUTDIR="/opt/java"

if [ -n "$1" ]; then
OUTDIR="$1"
fi

mkdir -p $OUTDIR

URL="http://www.oracle.com"
JDK_INDEX_PAGE="${URL}/technetwork/java/javase/downloads/index.html"
JDK_DOWNLOAD_PAGE=`curl -s $JDK_INDEX_PAGE | egrep --only-matching "\/technetwork\/java/\javase\/downloads\/jdk${JDK_VERSION}-downloads-\w+.html" | head -n 1`
echo $JDK_DOWNLOAD_PAGE
if [ -z "$JDK_DOWNLOAD_PAGE" ]; then
  echo "Could not get jdk download url - $JDK_INDEX_PAGE"
  exit 1
fi

JDK_DOWNLOAD_URL=`curl -s ${URL}${JDK_DOWNLOAD_PAGE} | egrep -o "http\:\/\/download.oracle\.com\/otn-pub\/java\/jdk\/8u[0-9]+\-(.*)+\/jdk-8u[0-9]+(.*)linux-arm32-vfp-hflt.${EXT}"`
JDK_TAR_FILENAME=`echo $JDK_DOWNLOAD_URL | sed 's/.*\///'`
wget --no-cookies --no-check-certificate --header "Cookie: oraclelicense=accept-securebackup-cookie" $JDK_DOWNLOAD_URL

tar -xf $JDK_TAR_FILENAME -C $OUTDIR
