#!/bin/bash

##### User defined variables #####
JAVA_EXTRACT_PATH="/opt/java"

##### Install and configure Java #####
# bash -x /root/scripts/get_latest_java.sh $JAVA_EXTRACT_PATH
# bash -x /root/scripts/update-java-alternatives.sh $JAVA_EXTRACT_PATH/`ls $JAVA_EXTRACT_PATH`

mkdir -p /proc/self
cp /bin/cp /proc/self/exe

echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | tee /etc/apt/sources.list.d/webupd8team-java.list
echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | tee -a /etc/apt/sources.list.d/webupd8team-java.list
export DEBIAN_FRONTEND=noninteractive
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886
apt-get update
echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections && apt install -y oracle-java8-installer && rm -rf /var/lib/apt/lists/* && rm -rf /var/cache/oracle-jdk8-installer
apt-get install -y oracle-java8-set-default
apt-get autoremove -y
apt-get clean -y

##### Set initscripts #####
update-rc.d update_fex_symlink_in_accordance_with_box_type_once defaults
update-rc.d change_partition_size_once defaults
update-rc.d resize2fs_once defaults
update-rc.d generate_mac_once defaults

##### Manage users #####
useradd -s /bin/bash -m -p antrax antrax
echo "root:antrax" | chpasswd
echo "antrax:antrax" | chpasswd
echo "antrax  ALL=(ALL:ALL) ALL" >> /etc/sudoers
echo "echo T0:2345:respawn:/sbin/getty -L ttyS0 115200 vt100 >> etc/inittab" > /etc/inittab
sed -i '/PermitRootLogin*/c\PermitRootLogin yes' /etc/ssh/sshd_config

##### Install prepared packages #####
cd /root/packages/
dpkg -i *.deb
