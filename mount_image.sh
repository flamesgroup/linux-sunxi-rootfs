#!/bin/bash

LOOPNAME=`kpartx -v -a $1 | grep -ohE "loop[0-9] " | head -n 1 | grep -ohE "loop[0-9]"`
mount -t ext2 /dev/mapper/"$LOOPNAME"p1 /mnt/boot
mount -t ext4 /dev/mapper/"$LOOPNAME"p2 /mnt/root
