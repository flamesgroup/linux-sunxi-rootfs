#!/bin/bash

sfdisk --force $1 < sd.layout
LOOPNAME=`kpartx -v -a $1 | grep -ohE "loop[0-9] " | head -n 1 | grep -ohE "loop[0-9]"`
mkfs.ext2 /dev/mapper/"$LOOPNAME"p1
mkfs.ext4 /dev/mapper/"$LOOPNAME"p2
dd if=u-boot-sunxi-with-spl.bin of=/dev/$LOOPNAME bs=1024 seek=8
kpartx -v -d $1
