#!/bin/bash

##### locales configuration #####
DEBIAN_FRONTEND=noninteractive apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install -y locales
locale-gen en_US.UTF-8
DEBIAN_FRONTEND=noninteractive dpkg-reconfigure locales
##### Install base packages #####
DEBIAN_FRONTEND=noninteractive apt-get install -y openssh-server openvpn screen unzip libopus0 psmisc zip jsvc watchdog cpufrequtils curl netplug fake-hwclock chrony i2c-tools ca-certificates iozone3
##### Install text editors and utils #####
DEBIAN_FRONTEND=noninteractive apt-get install -y bc xz-utils elfutils mc vim htop sudo tcpdump netcat
systemctl disable ntp
systemctl enable netplug fake-hwclock chrony ssh # Execute actions on Ethernet cable plug/unplug
